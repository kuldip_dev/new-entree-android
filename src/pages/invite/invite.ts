import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Contacts, ContactFieldType, ContactFindOptions } from '@ionic-native/contacts';

@IonicPage()
@Component({
  selector: 'page-invite',
  templateUrl: 'invite.html',
  providers: [Contacts]
})
export class InvitePage {
  allContacts:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private contacts: Contacts) {
  }

  fetchDeviceContacts() {
    this.contacts.find(["displayName", "phoneNumbers"], {multiple: true}).then((contacts) => {
      this.allContacts = contacts;
      console.log(contacts);
     })
  }

  ionViewDidLoad() {
    this.fetchDeviceContacts();
  }

}
