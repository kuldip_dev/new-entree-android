import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import * as firebase from 'firebase';
import { GroupPage } from '../group/group';
import { BackendManager } from '../../providers/backend-manager/backend-manager';
import { NewGroupPage } from '../new-group/new-group';

@IonicPage()
@Component({
  selector: 'page-groups',
  templateUrl: 'groups.html',
})
export class GroupsPage {
  userID:string;
  groups = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, public backendManager: BackendManager) {
    this.userID = backendManager.userID;
  }

  getUserGroups() {
    let groupsDBRef = firebase.database().ref('users/' + this.userID + '/groups');
    groupsDBRef.on('value', resp => {
      let groupIDs = Object.keys(resp.val());
      groupIDs.forEach((groupID, index) => {
        firebase.database().ref('groups/' + groupID + '/groupName').on('value', name => {
          this.groups[index] = {'groupID': groupID, 'groupName': name.val(), 'lastMessage': ''};
        });

        let lastMessageRef = firebase.database().ref('groups/' + groupID + '/messages');
        lastMessageRef.orderByChild('timestamp').limitToLast(1).on('value', resp => {

          let messageObj = resp.val();
          if (messageObj != null) {
            let messageID = Object.keys(messageObj)[0];
            if (messageObj[messageID].type == 'restaurant') {
              firebase.database().ref('groups/' + groupID + '/restaurants/' + messageObj[messageID].text + '/name').once('value', restaurantName => {
                if (messageObj[messageID].sender == this.userID) {
                  this.groups[index]['lastMessage'] = "You suggested " + restaurantName.val();
                }
                else {
                  firebase.database().ref('users/' + messageObj[messageID].sender + '/name').once('value', username => {
                    this.groups[index]['lastMessage'] = username.val() + " suggested " + restaurantName.val();
                  });
                }
              });
            }
            else {
              this.groups[index]['lastMessage'] = messageObj[messageID].text;
            }
          };
        });
      });
    });
  }

  groupTapped(group) {
    console.log(group);
    this.navCtrl.push(GroupPage, group);
  }

  newGroupTapped() {
    this.navCtrl.push(NewGroupPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GroupsPage');
    this.getUserGroups();



  }

}
