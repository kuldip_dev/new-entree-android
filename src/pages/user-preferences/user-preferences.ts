import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MasterDataProvider } from '../../providers/master-data/master-data';
import { TabsPage } from '../tabs/tabs';
import { BackendManager } from '../../providers/backend-manager/backend-manager';

@IonicPage()
@Component({
  selector: 'page-user-preferences',
  templateUrl: 'user-preferences.html',
})
export class UserPreferencesPage {
  
  cuisines = [];
  meats = [];

  userCuisines = {};
  userMeats = {};
  isVegetarian:boolean = false;

  showResetButton = false;

  index = 0;
  constructor(public navCtrl: NavController, public navParams: NavParams, public masterData: MasterDataProvider, public backendManager: BackendManager) {
  }

  removeBubble(bubbleID) {
    let index = this.cuisines.indexOf(bubbleID);
    if (index > -1) {
      delete this.cuisines[index];
      this.userCuisines[bubbleID] = 0;
    }

    let index2 = this.meats.indexOf(bubbleID);
    if (index2 > -1) {
      delete this.meats[index2];
      this.userMeats[bubbleID] = 0;
    }
  }

  resetBubbles() {
    if (this.index == 0) {
      this.cuisines = Object.keys(this.masterData.allCuisines);
      this.userCuisines = {};
    }
    else {
      this.meats = Object.keys(this.masterData.allMeats);
      this.userMeats = {};
      this.isVegetarian = false;
    }
    this.showResetButton = false;
  }

  bubbleTapped(bubbleID) {
    if (bubbleID in this.masterData.allCuisines) {
      if (bubbleID in this.userCuisines) {
        if (this.userCuisines[bubbleID] == 1.0) {
          delete this.userCuisines[bubbleID];
        }
        else {
          this.userCuisines[bubbleID] = 1.0;
        }
      }
      else {
        this.userCuisines[bubbleID] = 0.9;
      }
    }
    else if (bubbleID in this.masterData.allMeats) {
      if (bubbleID in this.userMeats) {
        if (this.userMeats[bubbleID] == 1.0) {
          delete this.userMeats[bubbleID];
        }
        else {
          this.userMeats[bubbleID] = 1.0;
        }
      }
      else {
        this.userMeats[bubbleID] = 0.9;
      }
    }
    if (this.index == 0 && Object.keys(this.userCuisines).length > 0) {
      this.showResetButton = true
    }
    else if (this.index == 1 && Object.keys(this.userMeats).length > 0) {
      this.showResetButton = true;
    }
    else {
      this.showResetButton = false;
    }
  }

  loadBubbles(){
    if (this.index == 0) {
      this.cuisines = Object.keys(this.masterData.allCuisines);
    }
    else {
      this.cuisines = [];
      this.meats = Object.keys(this.masterData.allMeats);
    }
  }

  vegetarianTapped() {
    this.isVegetarian = true;
    this.userMeats = {};
    this.createUser();
  }

  private createUser() {
    let user = {'isVegetarian': this.isVegetarian, 'meatWeights': this.userMeats, 'cuisineWeights': this.userCuisines};
    console.log(user);
    this.backendManager.createUser(user).subscribe(status => {
      if (status == true) {
        this.navCtrl.setRoot(TabsPage);
      }
    });
  }

  nextTapped() {
    if (this.index == 0) {
      if (Object.keys(this.userCuisines).length < 4) {
        alert('Please select at least 4 cuisines that you eat sometimes or often. You can always change your preferences in your profile.');
      }
      else {
        this.index += 1;
        this.showResetButton = false;
        this.loadBubbles();
      }
    }
    else if (this.index == 1) {
      if ((Object.keys(this.userMeats).length < 3) && (this.isVegetarian == false)) {
        alert('Please select at least 3 meats that you eat sometimes or often. You can always change your preferences in your profile.');
      }
      else {
        this.createUser();
      }
    }
  }

  backTapped() {
    if (this.index == 1) {
      this.index -= 1;
      this.loadBubbles();
    }
  }

  ionViewDidLoad() {
    this.loadBubbles();
  }

}
