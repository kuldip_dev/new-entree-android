import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserPreferencesPage } from './user-preferences';

@NgModule({
  declarations: [
    // UserPreferencesPage,
  ],
  imports: [
    IonicPageModule.forChild(UserPreferencesPage),
  ],
})
export class UserPreferencesPageModule {}
