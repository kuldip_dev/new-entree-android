import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Dish } from '../../models/dish/dish';
import { RestaurantDetailPage } from '../restaurant-detail/restaurant-detail';

/**
 * Generated class for the DishDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dish-detail',
  templateUrl: 'dish-detail.html',
})
export class DishDetailPage {
  dish: Dish;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.dish = navParams.data;
    this.dish.getDishDetails();
  }

  restaurantSelected(restaurant) {
    this.navCtrl.push(RestaurantDetailPage, restaurant);
  }
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad DishDetailPage');
  }

}
