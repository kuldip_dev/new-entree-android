import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Restaurant} from '../../models/restaurant/restaurant'

@IonicPage()
@Component({
  selector: 'page-restaurant-photos',
  templateUrl: 'restaurant-photos.html',
})

export class RestaurantPhotosPage {
  restaurant: Restaurant;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.restaurant = this.navParams.data;
  }

  ionViewDidLoad() {
  }

}
