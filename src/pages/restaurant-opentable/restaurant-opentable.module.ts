import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RestaurantOpentablePage } from './restaurant-opentable';

@NgModule({
  declarations: [
    // RestaurantOpentablePage,
  ],
  imports: [
    IonicPageModule.forChild(RestaurantOpentablePage),
  ],
})
export class RestaurantOpentablePageModule {}
