import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Restaurant } from '../../models/restaurant/restaurant';
import { DomSanitizer } from '@angular/platform-browser';

@IonicPage()
@Component({
  selector: 'page-restaurant-opentable',
  templateUrl: 'restaurant-opentable.html',
})
export class RestaurantOpentablePage {
  restaurant:Restaurant;
  sanitizedURL;
  
  constructor(public navCtrl: NavController, public navParams: NavParams, public sanitizer: DomSanitizer) {
    this.restaurant = navParams.data;
  }

  ionViewDidLoad() {
    this.sanitizedURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.restaurant.reservationURL);
  }

}
