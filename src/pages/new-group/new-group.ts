import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import * as firebase from 'firebase';
import { BackendManager } from '../../providers/backend-manager/backend-manager';
import { GroupsPage } from '../groups/groups';
import { GroupPage } from '../group/group';
import { InvitePage } from '../invite/invite';

@IonicPage()
@Component({
  selector: 'page-new-group',
  templateUrl: 'new-group.html',
})
export class NewGroupPage {

  messages = [];

  userID:string;
  userName:string;

  questionID:string = "-LDn8bIOQ1rHj_JjcJJe";

  questionIndex = 0;

  allContextAttributes = {};

  newGroupData = {};
  groupContext = {};

  constructor(public navCtrl: NavController, public navParams: NavParams, public backendManager: BackendManager) {
    this.userID = backendManager.userID;
    this.userName = backendManager.userName;
  }


  loadNextMessage() {
    let initialQuestionRef = firebase.database().ref('chatbotQuestions/' + this.questionID);
    initialQuestionRef.on('value', resp => {
      
      let questionObj = resp.val();
      
      var choices = [];
      questionObj.choices.forEach(choiceID => {
        choices.push({'choiceID': choiceID, 'choiceName': this.allContextAttributes[choiceID].name});
      });
      this.messages.push({"sender": "entree", "text": questionObj.question, "choices": choices});
      
    });
  }
  
  responseSelected(choice) {
    if (this.questionIndex == 0) {
      this.newGroupData['groupType'] = choice.choiceID;
    }
    this.messages.push({"sender": this.userID, "text": choice.choiceName});
    if (this.allContextAttributes[choice.choiceID].nextQuestionID != null) {
      this.questionIndex += 2;
      this.groupContext[choice.choiceID] = true;
      this.questionID = this.allContextAttributes[choice.choiceID].nextQuestionID;
      this.loadNextMessage();
    }
    else {
      this.questionIndex += 1;
      this.groupContext[choice.choiceID] = true;

      let members = {};
      members[this.userID] = {'admin': true};

      this.newGroupData['groupName'] = this.allContextAttributes[this.newGroupData['groupType']].defaultGroupName;
      this.newGroupData['members'] = members;
      
      this.newGroupData['context'] = this.groupContext;

      firebase.database().ref('groups/').push(this.newGroupData, err=>{console.log(err)}).then(snapshot => {
        firebase.database().ref('users/' + this.userID + '/groups/' + snapshot.key).set(true).then(g => {
          let newMessage = {"sender": "entree", "text": "Successfully created the group!", "type": "notification", "timestamp": Math.floor(new Date().getTime()/1000)}
          firebase.database().ref('groups/' + snapshot.key + '/messages').push(newMessage, err=>{console.log(err)});
          this.navCtrl.setRoot(GroupsPage);
          this.navCtrl.push(GroupPage, {'groupID': snapshot.key, 'groupName': this.allContextAttributes[this.newGroupData['groupType']].defaultGroupName});
        });
      });
    }
  } 

  ionViewDidLoad() {
    let allContextAttributesRef = firebase.database().ref('contextAttributes');
    allContextAttributesRef.on('value', resp => { 
      this.allContextAttributes = resp.val();

      this.loadNextMessage();
    });
    
  }

}
