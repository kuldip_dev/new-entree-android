import { Component } from '@angular/core';

import { EntreePage } from '../entree/entree';
import { RestaurantsPage } from '../restaurants/restaurants';
import { GroupsPage } from '../groups/groups'
import { LoginPage } from '../login/login'
import { DeliveryPage } from '../delivery/delivery'
import { LocationPage } from '../location/location';
import { BackendManager } from '../../providers/backend-manager/backend-manager';
import { NewGroupPage } from '../new-group/new-group';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = EntreePage;
  tab2Root = RestaurantsPage;
  tab3Root = DeliveryPage;
  tab4Root:any;
  tab5Root = LocationPage;

  groupBadge = 1;

  constructor(public backendManager: BackendManager) {
    this.tab4Root = GroupsPage;
     if (backendManager.isUserLoggedIn == true) {
       this.tab4Root = GroupsPage;
     }
     else {
       this.tab4Root = LoginPage;
     }
  }
}
