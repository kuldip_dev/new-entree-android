import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { BackendManager } from '../../providers/backend-manager/backend-manager'
import { RestaurantDetailPage } from '../restaurant-detail/restaurant-detail'

import lodash from 'lodash'

@IonicPage()
@Component({
  selector: 'page-restaurants',
  templateUrl: 'restaurants.html',
})

export class RestaurantsPage {
  restaurants = [];
  filteredRestaurants = []
  selectedFilterIndex = null;
  
  availableCuisines = [];
  availableAttributes = [];
  selectedSegmentIndex = "attributes";

  selectedAttributes = [];
  selectedCuisines = [];
  

  availableFilters = ['Good for groups', 'Good for dates', 'Cheap eats', 'Reservations', 'Happy hour'];

  overlayHidden: boolean = true;

  constructor(public navCtrl: NavController, public navParams: NavParams, public backendManager: BackendManager, public loadingCtrl: LoadingController) {
  }

  filterRestaurants(filterName) {
    this.filteredRestaurants = [];
    this.restaurants.forEach(r => {
      if (r.filters.includes(filterName)) {
        this.filteredRestaurants.push(r);
      }
    });
  }

  filterRestaurantsByAttributes() {
    this.filteredRestaurants = [];
    this.restaurants.forEach(r => {
      if (r.attributes) {
        this.selectedAttributes.forEach(a => {
          
          // if (r.attributes.includes()
        });

        this.filteredRestaurants.push(r);
      }
    });
  }

  

  filterSelected(selectedIndex) {
    if (this.selectedFilterIndex == selectedIndex) {
      this.selectedFilterIndex = null;
      this.filteredRestaurants = this.restaurants;
    }
    else {
      this.selectedFilterIndex = selectedIndex;
      switch (selectedIndex) {
        case (0):
          this.filterRestaurants("Good for groups");
          break;
        case (1):
          this.filterRestaurants("Good for dates");
          break;
        case (2):
          this.filterRestaurants("Cheap eats");
          break;
        case (3):
          this.filterRestaurants("Reservations");
          break;
        case (4):
          this.filterRestaurants("Happy hour");
          break;
      }
    }
      console.log(this.selectedFilterIndex);
  }

  attributeFilterSelected(attribute) {
    let index = this.selectedAttributes.indexOf(attribute);
    if (index == -1) {
      this.selectedAttributes.push(attribute);
    }
    else {
      this.selectedAttributes.splice(index, 1);
    }
  }
  cuisineFilterSelected(cuisine) {
    let index = this.selectedCuisines.indexOf(cuisine);
    if (index == -1) {
      this.selectedCuisines.push(cuisine);
    }
    else {
      this.selectedCuisines.splice(index, 1);
    }
    this.filterRestaurantsByAttributes();
  
  }


  restaurantSelected(restaurant) {
    this.navCtrl.push(RestaurantDetailPage, restaurant);
  }

  hideOverlay() {
    if (this.overlayHidden == true) {
      this.overlayHidden = false;
    }
    else {
      this.overlayHidden = true;
    }
  }




  ionViewDidLoad() {
    let spinner = this.loadingCtrl.create({'content': 'Fetching nearby restaurants...', 'spinner': 'dots'});
    spinner.present();
    this.backendManager.getNearbyRestaurants().subscribe(nearbyRestaurants => {
      spinner.dismiss();
      if(nearbyRestaurants.length > 0) {
        this.restaurants = nearbyRestaurants;
        this.filteredRestaurants = this.restaurants;
        this.restaurants.forEach(r => {
          this.availableAttributes = this.availableAttributes.concat(r.attributes);
          this.availableCuisines = this.availableCuisines.concat(r.cuisine);
        });
        this.availableAttributes = lodash.uniq(this.availableAttributes);
        // this.availableAttributes = lodash.orderBy(this.availableAttributes, )
        this.availableCuisines = lodash.uniq(this.availableCuisines);
      }
      else {
        
      }

      
    });

  }

}
