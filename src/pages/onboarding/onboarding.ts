import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LocationManagerProvider } from '../../providers/location-manager/location-manager';
import { UserPreferencesPage } from '../user-preferences/user-preferences';
import { Firebase } from '@ionic-native/firebase';

@IonicPage()
@Component({
  selector: 'page-onboarding',
  templateUrl: 'onboarding.html',
})
export class OnboardingPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public locationManager: LocationManagerProvider, public firebase:Firebase) {
  }

  getStarted() {
    this.locationManager.initialize().then(didFetchLocation => {
      this.navCtrl.setRoot(UserPreferencesPage);
    });

  }
  ionViewDidLoad() {
    this.firebase.setScreenName("Onboarding page");
  }

}
