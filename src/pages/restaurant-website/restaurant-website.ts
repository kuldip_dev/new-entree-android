import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';
import { Restaurant } from '../../models/restaurant/restaurant';

/**
 * Generated class for the RestaurantWebsitePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-restaurant-website',
  templateUrl: 'restaurant-website.html',
})
export class RestaurantWebsitePage {
  restaurant:Restaurant;
  sanitizedURL;

  constructor(public navCtrl: NavController, public navParams: NavParams, public sanitizer: DomSanitizer) {
    this.restaurant = navParams.data;
  }

  ionViewDidLoad() {
    this.sanitizedURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.restaurant.restaurantURL);
  }

}
