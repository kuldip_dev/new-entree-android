import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RestaurantWebsitePage } from './restaurant-website';

@NgModule({
  declarations: [
    // RestaurantWebsitePage,
  ],
  imports: [
    IonicPageModule.forChild(RestaurantWebsitePage),
  ],
})
export class RestaurantWebsitePageModule {}
