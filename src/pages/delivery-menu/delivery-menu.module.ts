import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DeliveryMenuPage } from './delivery-menu';

@NgModule({
  declarations: [
    // DeliveryMenuPage,
  ],
  imports: [
    IonicPageModule.forChild(DeliveryMenuPage),
  ],
})
export class DeliveryMenuPageModule {}
