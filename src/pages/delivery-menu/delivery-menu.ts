import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';

import { DeliveryRestaurant } from '../../models/delivery/delivery'
import {DeliveryItemPage} from '../delivery-item/delivery-item'

@IonicPage()
@Component({
  selector: 'page-delivery-menu',
  templateUrl: 'delivery-menu.html',
})
export class DeliveryMenuPage {
  restaurant: DeliveryRestaurant;

  searchStringInput:string = "";

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController) {
    this.restaurant = navParams.data;
  }

  onSearchInput() {}

  onSearchCancel() {}

  menuItemTapped(dish) {
    let modal = this.modalCtrl.create(DeliveryItemPage, {'dish': dish, 'restaurant': this.restaurant});
    modal.present();
  }

  ionViewDidLoad() {
    this.restaurant.getRestaurantMenu();
  }

}
