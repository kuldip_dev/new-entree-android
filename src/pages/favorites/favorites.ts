import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DishDetailPage } from '../dish-detail/dish-detail';

/**
 * Generated class for the FavoritesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-favorites',
  templateUrl: 'favorites.html',
})
export class FavoritesPage {

  likedDishes = [];
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.likedDishes = navParams.data;
  }

  dishTapped(dish) {
    console.log(dish);
    this.navCtrl.push(DishDetailPage, dish);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FavoritesPage');
  }

}
