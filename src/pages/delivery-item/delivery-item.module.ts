import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DeliveryItemPage } from './delivery-item';

@NgModule({
  declarations: [
    // DeliveryItemPage,
  ],
  imports: [
    IonicPageModule.forChild(DeliveryItemPage),
  ],
})
export class DeliveryItemPageModule {}
