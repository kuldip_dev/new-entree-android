import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DeliveryRestaurant } from '../../models/delivery/delivery';
import { WheelSelector } from '@ionic-native/wheel-selector';

@IonicPage()
@Component({
  selector: 'page-delivery-item',
  templateUrl: 'delivery-item.html',
})
export class DeliveryItemPage {
  dish: Object;
  restaurant: DeliveryRestaurant;

  specialRequests:string = "";
  quantity:number = 1;

  constructor(public navCtrl: NavController, public navParams: NavParams, public wheelSelector:WheelSelector) {
    this.dish = navParams.get('dish');
    this.restaurant = navParams.get('restaurant');
  }

  choiceSelected(choice) {
    if (choice.choiceType === "DROPDOWN") {
      var options = [];
      choice.choiceOptions.forEach(element => {
        options.push({description: element.choiceOptionName, test:'123abc'});
      });
      this.wheelSelector.show({
        title: "",
        items:[
            options
        ],
        positiveButtonText: "Save",
        negativeButtonText: "Cancel"
      }).then(result => {
        console.log(result[0].description + ' at index: ' + result[0].index);
        console.log(result);
      },
      err => console.log('Error: ', err)
      );
    }
    else if (choice.choiceType === "RADIO") {}
    
  }

  ionViewDidLoad() {
    console.log(this.dish);
    console.log(this.restaurant);
  }
  
  closeButtonTapped() {
    this.navCtrl.pop();
  }
}
