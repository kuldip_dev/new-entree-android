import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Restaurant} from '../../models/restaurant/restaurant'
import { RestaurantPhotosPage } from '../restaurant-photos/restaurant-photos'
import { RestaurantMenuPage } from '../restaurant-menu/restaurant-menu'
import { RestaurantOpentablePage } from '../restaurant-opentable/restaurant-opentable';
import { RestaurantWebsitePage } from '../restaurant-website/restaurant-website';

import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  Marker,
  GoogleMapsAnimation,
  MyLocation
} from '@ionic-native/google-maps';

@IonicPage()
@Component({
  selector: 'page-restaurant-detail',
  templateUrl: 'restaurant-detail.html',
})
export class RestaurantDetailPage {
  restaurant: Restaurant;

  mapReady: boolean = false;
  map: GoogleMap;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.restaurant = this.navParams.data;
    this.loadMap();
  }
  
  callRestaurant() {

  }

  loadMap() {
    this.map = GoogleMaps.create('map_canvas', {
      camera: {
        target: {
          lat: this.restaurant.latitude,
          lng: this.restaurant.longitude
        },
        zoom: 18,
        tilt: 30
      }
    });

    // Wait the maps plugin is ready until the MAP_READY event
    this.map.one(GoogleMapsEvent.MAP_READY).then(() => {
      this.mapReady = true;
    });
  }

  viewMenu() {
    this.restaurant.getRestaurantMenu();
    this.navCtrl.push(RestaurantMenuPage, this.restaurant);
  }

  viewPhotos() {
    this.restaurant.getRestaurantPhotos();
    this.navCtrl.push(RestaurantPhotosPage, this.restaurant);
  }

  viewOpentable() {
    this.navCtrl.push(RestaurantOpentablePage, this.restaurant);
  }
  
  viewWebsite() {
    this.navCtrl.push(RestaurantWebsitePage, this.restaurant);
  }

  ionViewDidLoad() {
    this.restaurant.getPrimaryPhoto();
    this.restaurant.getRestaurantReviews();
  }

}
