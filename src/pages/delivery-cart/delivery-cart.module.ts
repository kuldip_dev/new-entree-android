import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DeliveryCartPage } from './delivery-cart';

@NgModule({
  declarations: [
    DeliveryCartPage,
  ],
  imports: [
    IonicPageModule.forChild(DeliveryCartPage),
  ],
})
export class DeliveryCartPageModule {}
