import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DeliveryRestaurant } from '../../models/delivery/delivery';

/**
 * Generated class for the DeliveryCartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-delivery-cart',
  templateUrl: 'delivery-cart.html',
})
export class DeliveryCartPage {

  restaurant: DeliveryRestaurant;
  
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DeliveryCartPage');
  }

}
