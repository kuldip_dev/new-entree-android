import { Component, ViewChild, ViewChildren, QueryList } from '@angular/core';
import {
  Direction,
  StackConfig,
  Stack,
  Card,
  ThrowEvent,
  DragEvent,
  SwingStackComponent,
  SwingCardComponent } from 'angular2-swing';

import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { BackendManager } from '../../providers/backend-manager/backend-manager';
import { DishDetailPage } from '../dish-detail/dish-detail';
import { Storage } from '@ionic/storage';
import { FavoritesPage } from '../favorites/favorites';


@IonicPage()
@Component({
  selector: 'page-entree',
  templateUrl: 'entree.html',
})

export class EntreePage {
  @ViewChild('myswing1') swingStack: SwingStackComponent;
  @ViewChildren('mycards1') swingCards: QueryList<SwingCardComponent>;

  dishes = [];
  favoritesCount = 0;
  likedDishes = [];

  stackConfig: StackConfig;
  showLikeLabel:boolean = false;
  showDislikeLabel:boolean = false;
  searchInputEnabled:boolean = false;
  searchInput:string = ""

  constructor(public navCtrl: NavController, public navParams: NavParams, public backendManager:BackendManager, public loadingCtrl: LoadingController, public storage: Storage) { 
    this.stackConfig = {
      allowedDirections: [Direction.LEFT, Direction.RIGHT],
      throwOutConfidence: (offsetX, offsetY, element) => {
        return Math.min(Math.abs(offsetX) / (element.offsetWidth/2), 1);
      },
      throwOutDistance: (d) => {
        return 800;
      }
    };
  }
  ignore() {}

  showFavorites() {
    this.navCtrl.push(FavoritesPage, this.likedDishes);
  }

  ngAfterViewInit() {
    
    this.swingStack.dragend.subscribe((event: DragEvent) => {this.showLikeLabel = false; this.showDislikeLabel = false;});

    this.swingStack.dragmove.subscribe((event: DragEvent) => {
      if (event.offset < 0) {
        this.showLikeLabel = false;
        this.showDislikeLabel = true;
      }
      
      else if (event.offset > 0) {
        this.showLikeLabel = true;
        this.showDislikeLabel = false;
      }
      else {
        this.showLikeLabel = false;
        this.showDislikeLabel = false;
      }
      
    });
  }

  selectDish(dish) {
    this.navCtrl.push(DishDetailPage, dish);
  }

  swipeDish(like: boolean) {
    let swipedDish = this.dishes.pop();

    if (like == true) {
      this.favoritesCount += 1;
      this.likedDishes.push(swipedDish);
      this.storage.set(this.backendManager.sessionID, this.likedDishes);
    }

    this.backendManager.dishSwipe(swipedDish, like ? 1 : 0);
    if (this.dishes.length <= 0) {
      this.getDishes();
    }
  }

  getFavoritesCount() {
    this.storage.get(this.backendManager.sessionID).then(
      data => {
        if (data != null) {
          console.log(data);
          this.favoritesCount = data.length;
          this.likedDishes = data;
        }
        else {
          this.favoritesCount = 0;
          this.likedDishes = [];
        }
      },
      error => {
        console.log(error);
        this.favoritesCount = 0;
        this.likedDishes = [];
      }
    );

  }

  getDishes() {
    let spinner = this.loadingCtrl.create({'content': 'Deciding your dishes...', 'spinner': 'dots'});
    spinner.present();
    this.backendManager.getDishes().subscribe(dishes => {
      spinner.dismiss();
      this.dishes = dishes;
      this.getFavoritesCount();
    });
  }

  ionViewDidLoad() {
    this.getDishes();
    
  }

}
