import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GroupsPage } from '../groups/groups';
import { BackendManager } from '../../providers/backend-manager/backend-manager';
import { NewGroupPage } from '../new-group/new-group';


import {AuthenticationProvider} from "../../providers/authentication/authentication";
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */



@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})

export class LoginPage {
  messages = [];

  isUserInputEnabled = false;
  actionMessage = "";
  actionType = "";
  textInput:string;

  userID:string;
  userName:string;
  phoneNumber:string;
  authCode:string;
  verifyAuthcode:string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public backendManager: BackendManager, public AuthProvider: AuthenticationProvider) {
    this.userID = this.backendManager.userID;
  }

  ionViewDidEnter() {
    this.messages = [];
    this.loadMessage(0);
  }

  saveUserInput() {
    if (this.actionType == "username") {
      this.userName = this.textInput;
      this.textInput = "";
      this.actionMessage = "";
      this.actionType = "";
      this.isUserInputEnabled = false;
      this.loadMessage(6);

    }
    else if (this.actionType == "phone") {
      this.phoneNumber = this.textInput;
      this.textInput = "";
      this.actionMessage = "";
      this.actionType = "";
      this.isUserInputEnabled = false;
      this.loadMessage(8);
       this.authCode = this.AuthProvider.loginWithPhoneNumber(this.phoneNumber);
       if (this.authCode){
         this.loadMessage(13);
      }
    }
    else if (this.actionType == 'auth') {
      this.verifyAuthcode = this.textInput;
      this.textInput = "";
      this.actionMessage = "";
      this.actionType = "";
      this.isUserInputEnabled = false;
      this.loadMessage(10);
    }
  }

  loadMessage(index) {
    switch (index) {
      case 1:
        this.messages.push({'text': "Welcome to groups! I'd love to tell you more. Groups lets you decide where to eat with your friends", 'userID': "entree", "userName": "Entrée"});
        setTimeout(this.loadMessage(2), 1000);
        break;
      case 2:
        this.messages.push({'text': "Woah, this is so cool!", 'userID': "friend1", "userName": "Friend 1"});
        setTimeout(this.loadMessage(3), 500);
        break;
      case 3:
        this.messages.push({'text': "Finally! We can all decide where we're going!", 'userID': "friend2", "userName": "Friend 2"});
        setTimeout(this.loadMessage(4), 2000);
        break;
      case 4:
        this.messages.push({'text': "Let's get started by telling me your name.", 'userID': "entree", "userName": "Entrée"});
        setTimeout(this.loadMessage(5), 500);
        break;
      case 5:
        this.isUserInputEnabled = true;
        this.actionMessage = "Your name: ";
        this.actionType = 'username'
        break;
      case 6:
        this.messages.push({'text': this.userName, "userID": this.userID});
        setTimeout(this.loadMessage(7), 500);
        break;
      case 7:
        this.messages.push({'text': "Hello " + this.userName + "! Welcome to groups. Next thing we need is your phone number. This allows your friends to easily find you", "userID": "entree"});
        this.isUserInputEnabled = true;
        this.actionMessage = "Phone number: ";
        this.actionType = 'phone';
        break;
      case 8:
        this.messages.push({'text': this.phoneNumber, "userID": this.userID});
        setTimeout(this.loadMessage(9), 500);
        break;
      case 9:
        this.messages.push({'text': "Got it! Just sent you a 4 digit code to verify your phone number", "userID": "entree"});
        this.isUserInputEnabled = true;
        this.actionMessage = "Auth code: ";
        this.actionType = 'auth'
        break;
      case 10:
        this.messages.push({'text': this.authCode, "userID": this.userID});
        if (this.verifyAuthcode == this.authCode){
          setTimeout(this.loadMessage(11), 500);
        }else{
          setTimeout(this.loadMessage(7), 500);
        }
        break;
      case 11:
        this.messages.push({'text': "You're all set!", "userID": "entree"});
        this.backendManager.isUserLoggedIn = true;
        this.backendManager.userName = this.userName;
        this.backendManager.createNewGroupUser(this.phoneNumber).then(success => {
          this.navCtrl.setRoot(GroupsPage);
        });
        break;
      case 12:
        this.messages.push({'text': "Hello " + this.userName + "! Sorry the 4 digit code to verify your phone number was wrong. Next thing we need is your phone number so we can sent code again", "userID": "entree"});
        this.isUserInputEnabled = true;
        this.actionMessage = "Phone number: ";
        this.actionType = 'phone';
        break;
      case 13:
        this.messages.push({'text': "Hello " + this.userName + "! Sorry the code was not able to send, try again later", "userID": "entree"});
        this.isUserInputEnabled = false;
        this.actionMessage = "Phone number: ";
        this.actionType = 'phone';
        break;
      default:
        this.messages.push({'text': "Hi... what is this?", 'userID': this.userID});
        setTimeout(this.loadMessage(1), 3000);
      }

    // this.messages.push();
  }

}
