import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Restaurant} from '../../models/restaurant/restaurant'


@IonicPage()
@Component({
  selector: 'page-restaurant-menu',
  templateUrl: 'restaurant-menu.html',
})
export class RestaurantMenuPage {
  restaurant: Restaurant;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.restaurant = navParams.data
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RestaurantMenuPage');
  }

}
