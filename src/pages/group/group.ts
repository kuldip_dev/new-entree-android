import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import * as firebase from 'firebase';
import { BackendManager } from '../../providers/backend-manager/backend-manager';
import { Restaurant } from '../../models/restaurant/restaurant';


@IonicPage()
@Component({
  selector: 'page-group',
  templateUrl: 'group.html',
})
export class GroupPage {
  userID:string;
  groupID:string;
  userName:string;
  groupName:string;

  messages = [];

  allMessages = [];
  userNames = {};
  isUserAdmin = false;

  showRestaurantPanel = false;
  showRestaurantOverlay = false;
  showRestaurantMessagesOnly = false;

  selectedRestaurant:Restaurant = null;

  selectedRestaurantBarButton = null;

  restaurantBarRestaurants = [];
  restaurantBarText = "";


  recommendedRestaurants = [];
  nearbyRestaurants = [];
  filteredNearbyRestaurants = [];

  restaurantMessages = {};
  restaurantMessagesCount = 0;

  restaurantVotes = {};

  messageInput = "";

  constructor(public navCtrl: NavController, public navParams: NavParams, public backendManager:BackendManager) {
    this.groupID = navParams.data.groupID;
    this.groupName = navParams.data.groupName;
    this.userID = this.backendManager.userID;
    this.userName = this.backendManager.userName;
  }

  viewRestaurantDetail(restaurant:Restaurant) {
    restaurant.getPrimaryPhoto();
    restaurant.getRestaurantReviews();
    this.selectedRestaurant = restaurant;
    this.showRestaurantOverlay = true;
  }
  closeRestaurantDetail() {
    this.selectedRestaurant = null;
    this.showRestaurantOverlay = false;
  }

  likeRestaurant(restaurant:Restaurant) {
    firebase.database().ref('groups/' + this.groupID + '/restaurants/' + restaurant.restaurantID + '/votes/' + this.userID).set(true, err=>{});
    let newMessage = {"sender": this.userID, "text": this.userName + " liked " + restaurant.name + "!", "type": "notification", "timestamp": Math.floor(new Date().getTime()/1000)}
    firebase.database().ref('groups/' + this.groupID + '/messages').push(newMessage, err=>{console.log(err)});
  }
  
  dislikeRestaurant(restaurant:Restaurant) {
    firebase.database().ref('groups/' + this.groupID + '/restaurants/' + restaurant.restaurantID + '/votes/' + this.userID).set(false, err=>{});
    let newMessage = {"sender": this.userID, "text": this.userName + " disliked " + restaurant.name + "!", "type": "notification", "timestamp": Math.floor(new Date().getTime()/1000)}
    firebase.database().ref('groups/' + this.groupID + '/messages').push(newMessage, err=>{console.log(err)});
    
  }

  recommendedRestaurantsButtonTapped(index:number) {
    console.log(index);
    console.log(this.selectedRestaurantBarButton);
    
    if (index == this.selectedRestaurantBarButton) {
      this.selectedRestaurantBarButton = null;
      this.showRestaurantPanel = false;
    }
    else {
      this.selectedRestaurantBarButton = index;
      if (index == 0) {
        this.restaurantBarRestaurants = this.recommendedRestaurants;
        this.restaurantBarText = "Recommended restaurants";
      }
      else if (index == 1) {
        this.restaurantBarRestaurants = this.nearbyRestaurants;
        this.restaurantBarText = "Nearby restaurants";
      }
      else if (index == 2) {
        this.restaurantBarRestaurants = [];
      }
      else if (index == 3) {
        this.restaurantBarRestaurants = [];
      }
      this.showRestaurantPanel = true;
    }

    console.log(index);
    console.log(this.selectedRestaurantBarButton);
  }
  
  sendMessage() {
    if (this.messageInput != "") {
      let newMessage = {"sender": this.userID, "text": this.messageInput, "type": "text", "timestamp": Math.floor(new Date().getTime()/1000)}
      firebase.database().ref('groups/' + this.groupID + '/messages').push(newMessage, err=>{console.log(err)});
      this.messageInput = "";
    }
  }

  sendRestaurant(restaurant:Restaurant) {
    let index = this.recommendedRestaurants.indexOf(restaurant);
    this.recommendedRestaurants.splice(index, 1);

    let newMessage = {"sender": this.userID, "text": restaurant.restaurantID, "type": "restaurant", "timestamp": Math.floor(new Date().getTime()/1000)}
    firebase.database().ref('groups/' + this.groupID + '/messages').push(newMessage, err=>{console.log(err)});
    firebase.database().ref('groups/' + this.groupID + '/restaurants/' + restaurant.restaurantID).set({'name': restaurant.name}, err=>{console.log(err)});
    
    // scroll to bottom
  }

  votesButtonTapped() {
    if (this.showRestaurantMessagesOnly == true) {
      this.messages = this.allMessages;
      this.showRestaurantMessagesOnly = false;
    }
    else {
      this.messages = this.allMessages.filter(m => m.type == 'restaurant');
      this.showRestaurantMessagesOnly = true;
    }

    this.showRestaurantPanel = false;
    
  }
  

  ionViewDidLoad() {
    let membersRef = firebase.database().ref('groups/' + this.groupID + '/members');
    membersRef.on('value', resp => {
      let userIDs = resp.val();
      Object.keys(userIDs).forEach(userID => {
        if (userID == this.userID) {
          if (userIDs[userID].admin == true) {
            this.isUserAdmin = true;
          }
        }

        firebase.database().ref('users/' + userID + '/name').on('value', n => {
          this.userNames[userID] = n.val();
        });
      });
    });
    

    let messagesRef = firebase.database().ref('groups/' + this.groupID + '/messages');
    messagesRef.orderByChild('timestamp').on('value', resp => {
      let messagesObj = resp.val();
      this.messages = [];
      this.allMessages = [];
      for (let key in messagesObj) {
        if (messagesObj[key]['type'] == 'restaurant') {
          if (!this.restaurantMessages.hasOwnProperty(messagesObj[key]['text'])) {
            this.backendManager.getRestaurantByID(messagesObj[key]['text']).subscribe(restaurant => {
              this.restaurantMessages[messagesObj[key]['text']] = restaurant;
            });
          }
          this.messages.push(Object.assign({messageID: key}, messagesObj[key], {userName: this.userNames[messagesObj[key]['sender']]}));
          this.allMessages.push(Object.assign({messageID: key}, messagesObj[key], {userName: this.userNames[messagesObj[key]['sender']]}));
        }
        else if (messagesObj[key]['type'] == 'text') {
          if (this.showRestaurantMessagesOnly == false) {
            this.messages.push(Object.assign({messageID: key}, messagesObj[key], {userName: this.userNames[messagesObj[key]['sender']]}));
          }
          this.allMessages.push(Object.assign({messageID: key}, messagesObj[key], {userName: this.userNames[messagesObj[key]['sender']]}));
        }
        else {
          if ((messagesObj[key]['type'] == 'notification') && (messagesObj[key]['sender'] != this.userID)) {
            if (this.showRestaurantMessagesOnly == false) {

              this.messages.push(Object.assign({messageID: key}, messagesObj[key], {userName: this.userNames[messagesObj[key]['sender']]}));
            }
            this.allMessages.push(Object.assign({messageID: key}, messagesObj[key], {userName: this.userNames[messagesObj[key]['sender']]}));
          }
        }

      }
    });

    let restaurantVotesRef = firebase.database().ref('groups/' + this.groupID + '/restaurants');
    restaurantVotesRef.on('value', resp => {
      let votesObj = resp.val();
      this.restaurantVotes = {};
      if (votesObj != null) {
        this.restaurantMessagesCount = Object.keys(votesObj).length;
      }
      for (let restaurantID in votesObj) {
        this.restaurantVotes[restaurantID] = {'isLiked': false, 'isDisliked': false, 'totalLiked': 0, 'totalDisliked': 0};
        
        if (votesObj[restaurantID].hasOwnProperty('votes')) {
          if (votesObj[restaurantID]['votes'].hasOwnProperty(this.userID)) {
            if (votesObj[restaurantID]['votes'][this.userID] == true) {
              this.restaurantVotes[restaurantID]['isLiked'] = true;
            }
            else {
              this.restaurantVotes[restaurantID]['isDisliked'] = true;
            }
          }
          
          for (let uID in votesObj[restaurantID]['votes']) {
            if (votesObj[restaurantID]['votes'][uID] == true) {
              this.restaurantVotes[restaurantID]['totalLiked'] += 1;
            }
            else {
              this.restaurantVotes[restaurantID]['totalDisliked'] += 1;

            }
          };
        }
      };
    });

    this.backendManager.getGroupRecommendedRestaurants(this.groupID).subscribe(restaurants => {this.recommendedRestaurants = restaurants});
    this.backendManager.getNearbyRestaurants().subscribe(restaurants => {this.nearbyRestaurants = restaurants;})


  }

}
