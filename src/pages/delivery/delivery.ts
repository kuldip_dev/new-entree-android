import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Content } from 'ionic-angular';
import { BackendManager } from '../../providers/backend-manager/backend-manager'
import { DeliveryMenuPage } from '../delivery-menu/delivery-menu'
import { DeliveryTopDish } from '../../models/delivery/delivery';
// import lodash from 'lodash'

@IonicPage()
@Component({
  selector: 'page-delivery',
  templateUrl: 'delivery.html',
})
export class DeliveryPage {
  @ViewChild('topDishScrollView') topDishScrollView: any;
  restaurants = [];
  filteredRestaurants = [];
  topDishes = [];
  
  filteredTopDishes = [];
  selectedTopDish = null;

  searchStringInput:string = "";

  constructor(public navCtrl: NavController, public navParams: NavParams, public backendManager: BackendManager, public loadingCtrl: LoadingController) {
  }

  onSearchInput(event){    
    this.filteredRestaurants = this.restaurants.filter(r => r.name.toLowerCase().indexOf(this.searchStringInput.toLowerCase()) !== -1);
    this.filteredTopDishes = this.topDishes.filter(r => r.dishName.toLowerCase().indexOf(this.searchStringInput.toLowerCase()) !== -1);
  }

  onSearchCancel() {
    this.filteredRestaurants = this.restaurants;
  }
  
  restaurantTapped(restaurant) {
    this.navCtrl.push(DeliveryMenuPage, restaurant);
  }

  topDishTapped(topDish:DeliveryTopDish){
    if (!this.selectedTopDish || this.selectedTopDish.dishID != topDish.dishID) {
      this.selectedTopDish = topDish;
      this.filteredRestaurants = this.restaurants.filter(r => topDish.dishID in r.dishes);
      this.filteredTopDishes = [];
      console.log(this.filteredTopDishes.length);
      this.filteredTopDishes = this.topDishes.filter(t => t.dishCuisineID == topDish.dishCuisineID && t != topDish);
      // console.log(this.filteredTopDishes.length);
      // let index = this.filteredTopDishes.indexOf(topDish);
      // this.filteredTopDishes.splice(index, 1);
      console.log(this.filteredTopDishes.length);
  
      this.topDishScrollView._scrollContent.nativeElement.scrollTo({ left: 0, top: 0, behavior: 'smooth' });
      
    }
    // else if () {
    //   this.selectedTopDish = topDish;
    //   this.filteredRestaurants = this.restaurants.filter(r => topDish.dishID in r.dishes);
    // }
    else {
      this.selectedTopDish = null;
      this.filteredRestaurants = this.restaurants;
      this.filteredTopDishes = this.topDishes;
    }
    console.log(topDish);
    console.log(this.selectedTopDish);
    
  }

  ionViewDidLoad() {
    let spinner = this.loadingCtrl.create({'content': 'Fetching nearby restaurants...', 'spinner': 'dots'});
    spinner.present();
    this.backendManager.getNearbyDeliveryRestaurants().subscribe(results => {
      console.log(results);
      this.restaurants = results.restaurants;
      this.topDishes = results.topDishes; 
      this.filteredTopDishes = results.topDishes;
      this.filteredRestaurants = this.restaurants;
      spinner.dismiss(); 
      
    });
  }

}
