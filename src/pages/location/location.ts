import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';

import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  Marker,
  GoogleMapsAnimation,
  MyLocation,
  Geocoder, GeocoderResult, ILatLng
} from '@ionic-native/google-maps';


@IonicPage()
@Component({
  selector: 'page-location',
  templateUrl: 'location.html',
})
export class LocationPage {

  mapReady: boolean = false;
  map: GoogleMap;

  recentLocations = [];
  selectedLat:number;
  selectedLng:number;

  latLng:any;

  currentlocation: any;

  //autocompleteItems: any;
  //autocomplete: any;
  //acService: any;
  //placesService: any;


  constructor(public navCtrl: NavController, public navParams: NavParams, public toastCtrl: ToastController) {
  }

  // ngOnInit() {
  //  this.acService = new google.maps.places.AutocompleteService();
  //  this.autocompleteItems = [];
  //  this.autocomplete = {
  //  query: ''
  //  };
  // }


  ionViewDidLoad() {
    this.loadMap();
  }


  // updateSearch() {

  //     console.log('modal > updateSearch');

  //     if (this.autocomplete.query == '') {
  //       this.autocompleteItems = [];
  //       return;
  //     }

  //     let self = this;
  //     let config = {
  //       //types:  ['geocode'], // other types available in the API: 'establishment', 'regions', and 'cities'
  //       input: this.autocomplete.query,
  //       componentRestrictions: {  }
  //     }

  //     this.acService.getPlacePredictions(config, function (predictions, status) {
  //     console.log('modal > getPlacePredictions > status > ', status);
  //     self.autocompleteItems = [];
  //       predictions.forEach(function (prediction) {
  //       self.autocompleteItems.push(prediction);
  //       });
  //     });
  // }



  loadMap() {
    this.map = GoogleMaps.create('map_canvas', {
      camera: {
        target: {
          lat: 43.0741704,
          lng: -89.3809802
        },
        zoom: 18,
        tilt: 30
      }
    });

    // Wait the maps plugin is ready until the MAP_READY event
    this.map.one(GoogleMapsEvent.MAP_READY).then(() => {
      this.mapReady = true;
    });
  }

  onButtonClick() {
    if (!this.mapReady) {
      this.showToast('map is not ready yet. Please try again.');
      return;
    }
    this.map.clear();

    // Get the location of you
    this.map.getMyLocation()
      .then((location: MyLocation) => {
        alert(JSON.stringify(location, null ,2));
        this.latLng = location.latLng;


        // Move the map camera to the location with animation
        return this.map.animateCamera({
          target: location.latLng,
          zoom: 17,
          tilt: 30
        }).then(() => {

          // add a marker
          return this.map.addMarker({
            title: '@ionic-native/google-maps plugin!',
            snippet: 'This plugin is awesome!',
            position: location.latLng,
            animation: GoogleMapsAnimation.BOUNCE
          });

        })
      }).then((marker: Marker) => {
        // show the infoWindow
        let latLng: ILatLng = this.latLng;
        let marker2: Marker = this.map.addMarkerSync({
          "position": latLng
        });

        // Latitude, longitude -> address
        Geocoder.geocode({
          "position": latLng
        }).then((results: GeocoderResult[]) => {
          if (results.length == 0) {
            // Not found
            return null;
          }
          let address: any = [
            results[0].subThoroughfare || "",
            results[0].thoroughfare || "",
            results[0].locality || "",
            results[0].adminArea || "",
            results[0].postalCode || "",
            results[0].country || ""].join(", ");
          this.currentlocation = results[0].country;
          marker.setTitle(address);
          marker.showInfoWindow();
        });
        //marker.showInfoWindow();

        // If clicked it, display the alert
        marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
          this.showToast('clicked!');
        });
      });
  }

  showToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 2000,
      position: 'middle'
    });

    toast.present(toast);
  }

}
