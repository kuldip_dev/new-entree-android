import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http'; 
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { WheelSelector } from '@ionic-native/wheel-selector';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { LongPressModule } from 'ionic-long-press';

import { Geolocation } from '@ionic-native/geolocation'
import { IonicStorageModule } from '@ionic/storage';
import { Firebase } from '@ionic-native/firebase'
import { Contacts } from '@ionic-native/contacts'

import { GooglePlacesAutocompleteComponentModule } from 'ionic2-google-places-autocomplete';

import { SwingModule } from 'angular2-swing'

//main tabs
import { TabsPage } from '../pages/tabs/tabs';
import { EntreePage } from '../pages/entree/entree';
import { RestaurantsPage } from '../pages/restaurants/restaurants';
import { DeliveryPage } from '../pages/delivery/delivery';
import { GroupsPage } from '../pages/groups/groups';

// models
import { Restaurant } from '../models/restaurant/restaurant';
import { Dish } from '../models/dish/dish';
import { DeliveryRestaurant } from '../models/delivery/delivery';

// backend
import { BackendManager } from '../providers/backend-manager/backend-manager';
import { LocationManagerProvider } from '../providers/location-manager/location-manager';
import { MasterDataProvider } from '../providers/master-data/master-data';

// other pages 
import { RestaurantDetailPage } from '../pages/restaurant-detail/restaurant-detail';
import { RestaurantPhotosPage } from '../pages/restaurant-photos/restaurant-photos';
import { UserPreferencesPage } from '../pages/user-preferences/user-preferences';
import { LoginPage } from '../pages/login/login';
import { DeliveryMenuPage } from '../pages/delivery-menu/delivery-menu';
import { DeliveryItemPage } from '../pages/delivery-item/delivery-item';
import { LocationPage } from '../pages/location/location';
import { GroupPage } from '../pages/group/group';
import { FcmProvider } from '../providers/fcm/fcm';
import { DishDetailPage } from '../pages/dish-detail/dish-detail';
import { RestaurantMenuPage } from '../pages/restaurant-menu/restaurant-menu';
import { RestaurantOpentablePage } from '../pages/restaurant-opentable/restaurant-opentable';
import { RestaurantWebsitePage } from '../pages/restaurant-website/restaurant-website';
import { AuthenticationProvider } from '../providers/authentication/authentication';
import { FavoritesPage } from '../pages/favorites/favorites';
import { OnboardingPage } from '../pages/onboarding/onboarding';
import { NewGroupPage } from '../pages/new-group/new-group';
import { InvitePage } from '../pages/invite/invite';
import { AnalyticsManagerProvider } from '../providers/analytics-manager/analytics-manager';



@NgModule({
  declarations: [
    MyApp,
    OnboardingPage,
    EntreePage,
    RestaurantsPage,
    DeliveryPage,
    GroupsPage,
    GroupPage,
    NewGroupPage,
    LocationPage,
    LoginPage,
    InvitePage,
    FavoritesPage,
    DishDetailPage,
    RestaurantDetailPage,
    RestaurantPhotosPage,
    RestaurantMenuPage,
    RestaurantOpentablePage,
    RestaurantWebsitePage,
    UserPreferencesPage,
    DeliveryMenuPage,
    DeliveryItemPage,
    TabsPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    SwingModule,
    LongPressModule,
    GooglePlacesAutocompleteComponentModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    OnboardingPage,
    TabsPage, 
    EntreePage,
    RestaurantsPage,
    DeliveryPage,
    GroupsPage,
    GroupPage,
    NewGroupPage,
    LocationPage,
    LoginPage,
    InvitePage,
    FavoritesPage,
    DishDetailPage,
    RestaurantDetailPage,
    RestaurantPhotosPage,
    RestaurantMenuPage,
    RestaurantOpentablePage,
    RestaurantWebsitePage,
    DeliveryMenuPage,
    DeliveryItemPage,
    UserPreferencesPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Geolocation,
    WheelSelector,
    ScreenOrientation,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Firebase,
    Restaurant, DeliveryRestaurant,
    BackendManager,
    LocationManagerProvider,
    MasterDataProvider,
    FcmProvider,
    AuthenticationProvider,
    AnalyticsManagerProvider
  ]
})
export class AppModule {}
