import { Component } from '@angular/core';
import { Platform, Tab } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';
import { OnboardingPage } from '../pages/onboarding/onboarding'
import * as firebase from 'firebase';
import { LocationManagerProvider } from '../providers/location-manager/location-manager';
import { MasterDataProvider } from '../providers/master-data/master-data';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { Storage } from '@ionic/storage';
import { BackendManager } from '../providers/backend-manager/backend-manager';

const config = {
  apiKey: "AIzaSyCv7rqbGB6yfykprRv5ZGdNK7UGbb4Y9eI",
  authDomain: "entree-mobile-app.firebaseapp.com",
  databaseURL: "https://entree-mobile-app.firebaseio.com",
  projectId: "entree-mobile-app",
  storageBucket: "entree-mobile-app.appspot.com",
  messagingSenderId: "603302651315"
};

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any;


  testing = true;

  constructor (
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    locationManager:LocationManagerProvider,
    masterData:MasterDataProvider,
    screenOrientation: ScreenOrientation,
    storage: Storage,
    backendManager: BackendManager
  )
  {
    platform.ready().then(() => {

      screenOrientation.lock(screenOrientation.ORIENTATIONS.PORTRAIT);
      statusBar.backgroundColorByHexString("#ca1d1d");
      splashScreen.hide();

      firebase.initializeApp(config);
      masterData.initialize();
      backendManager.initializeCache();

      if (this.testing == true) {
        if (platform.is('android')) {
          backendManager.userID = "8b291f9a-4d5e-4c96-97fe-4cbddf56a50c"
        }
        else {
          backendManager.userID = "4678570d-796b-4e59-b51d-03a993fcbffd"

        }
        locationManager.initialize().then(didFetchLocation => {
          this.rootPage = TabsPage;
        });
      }
      else {
        storage.get('isFirstTime').then(isFirstTime => {
          if (isFirstTime != null && isFirstTime == true) {
            locationManager.initialize().then(didFetchLocation => {
              this.rootPage = TabsPage;
            });
          }
          else {
            this.rootPage = OnboardingPage;
          }
        });
      }
    });

  }
}
