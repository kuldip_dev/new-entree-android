import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation'
/*
  Generated class for the LocationManagerProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LocationManagerProvider {

  latitude: number;
  longitude: number;

  constructor(public http: HttpClient, public geolocation: Geolocation) {

  }

  initialize() {
    return this.geolocation.getCurrentPosition({ enableHighAccuracy: true, timeout: 5000, maximumAge: 0 }).then((resp) => {
    console.log(resp ,'Location Response');
      this.latitude = resp.coords.latitude;
      this.longitude = resp.coords.longitude;
      console.log(this.latitude,'Location latitude');
      console.log(this.longitude,'Location longitude');
      return true;

     }).catch((error) => {
       console.log('Error getting location', JSON.stringify(error));
       alert('Your device location is not available. Please turn on your GPS.');
       this.latitude = 41.878114;
       this.longitude = -87.629798;
       return false;
     });

  }


}
