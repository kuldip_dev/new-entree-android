import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http, Headers, RequestOptions } from '@angular/http';

import { Restaurant } from '../../models/restaurant/restaurant'
import { Dish } from '../../models/dish/dish'
import { DeliveryRestaurant, DeliveryTopDish } from '../../models/delivery/delivery'
import { LocationManagerProvider } from '../location-manager/location-manager'
import { MasterDataProvider } from '../master-data/master-data'
import { Storage } from '@ionic/storage';

// import * as moment from 'moment'
import moment from 'moment-timezone'
import * as firebase from 'firebase';


@Injectable()
export class BackendManager {

  baseURL = "https://ilbdbmc8sb.execute-api.us-east-1.amazonaws.com/v2/";
  baseDeliveryURL = "https://ur5sumi7vi.execute-api.us-east-1.amazonaws.com/v2/"
  apiKey = "50YL0BNXlc6I8NjvlXzxE3KmHFd829sJ24AwkXuO";

  constructor(public http: Http, public locationManager: LocationManagerProvider, public masterData: MasterDataProvider, public storage:Storage) {
    // this.getUserID();
    this.getSessionID();
  }

  userID:string;
  sessionID:string;
  isUserLoggedIn:boolean = false;
  userName:string = "Prajoth";

  getUserID() {
    this.storage.get('userID').then(
      data => {
        if (data) {
          this.userID = data;
        }
      },
      error => {console.log(error);return ''}
    );
  }

  getSessionID() {
    this.storage.get('sessionID').then(
      data => {
        if (data) {
          this.sessionID = data;
        }
      },
      error => {console.log(error);return ''}
    );
  }

  createUser(user) {

    let headers = new Headers()
    headers.append('x-api-key', this.apiKey);
    let options = new RequestOptions({ headers: headers });

    let url =  this.baseURL + "users/create"
    return this.http.post(url, JSON.stringify(user), options).map(data => {
      try {
        let response = data.json();
        switch (data.status) {
          case 200:
            this.userID = response.userID;
            this.storage.set('userID', response.userID);
            return true;
          default:
            console.log(data);
            return false;
        }
      }
      catch(error) {return false};
    });

  }

  initializeCache() {
    if (this.userID && this.sessionID && this.locationManager.latitude && this.locationManager.longitude) {
      let params = {
        'userID': this.userID,
        'sessionID': this.sessionID,
        'latitude': this.locationManager.latitude,
        'longitude': this.locationManager.longitude
      }
      let headers = new Headers()
      headers.append('x-api-key', this.apiKey);
      let options = new RequestOptions({ headers: headers });

      let url =  this.baseDeliveryURL + "cache/initialize"
      this.http.post(url, JSON.stringify(params), options).subscribe(response => {if (response.json().status != 200) {console.log(response);}});

    }
  }


  getDishes() {

    let headers = new Headers()
    headers.append('x-api-key', this.apiKey);
    let options = new RequestOptions({ headers: headers });

    let url =  this.baseURL + "dishes"

    var params = {'userID': this.userID, 'latitude': this.locationManager.latitude, "longitude": this.locationManager.longitude, 'cuisines':[], 'meats':[]};

    let currentTimestamp = new Date().getTime()/1000;
    params['timestamp'] = currentTimestamp;

    let timezoneName = Intl.DateTimeFormat().resolvedOptions().timeZone;
    params['timeZoneName'] = timezoneName;
    params['timeZone'] = moment.tz(timezoneName).zoneAbbr()


    if (this.sessionID != null) {
      params['sessionID'] = this.sessionID;
    }

    return this.http.post(url, JSON.stringify(params), options).map(data => {

      let dishes = [];
      try {
        let response = data.json()
        switch (response.status) {
          case 200:
            let sessionID = response.sessionID;

            let previousSessionID = this.sessionID;
            this.storage.set("sessionID", sessionID);
            this.sessionID = sessionID;

            if (!previousSessionID || this.sessionID != previousSessionID) {
              this.initializeCache();
            }

            let results = response.result;
            results.forEach(result => {
              let dish = new Dish(this.http);
              dish.initialize(result);
              dishes.push(dish);
            });
            return dishes;

          case 404:
            console.log('No results in your area!');
            break;
          default:
            console.log(data);
            console.log('Something went wrong');
        }
      }
      catch(error) {return []};

      });



  }

  dishSwipe(dish: Dish, swipeAction: number) {
    let headers = new Headers()
    headers.append('x-api-key', this.apiKey);
    let options = new RequestOptions({ headers: headers });

    let url =  this.baseURL + "dishes/swipe"

    var params = {
      'userID': this.userID,
      'sessionID': this.sessionID,
      'dishID': dish.dishID,
      'action': swipeAction,
      'dishCuisineID': dish.cuisineID,
      'dishMeatID': dish.meatID
    };

    let currentTimestamp = new Date().getTime()/1000;
    params['Timestamp'] = currentTimestamp;

    let timezoneName = Intl.DateTimeFormat().resolvedOptions().timeZone;
    params['timeZoneName'] = timezoneName;
    params['timeZone'] = moment.tz(timezoneName).zoneAbbr()

    this.http.post(url, JSON.stringify(params), options).subscribe(data => {
      try {
        let response = data.json()
        switch (response.status) {
          case 200:
            break;
          default:
            // console.log(data);
            console.log('Something went wrong');

        }
      }
      catch(error) {console.log(error);};

      });

  }

  getNearbyRestaurants() {
    let headers = new Headers()
    headers.append('x-api-key', this.apiKey);
    let options = new RequestOptions({ headers: headers });

    let url =  this.baseURL + "restaurants"
    let params = {'userID': this.userID, "sessionID": this.sessionID, 'latitude': this.locationManager.latitude, "longitude": this.locationManager.longitude, "timeZoneName": "", "timeZone": ""}
    return this.http.post(url, JSON.stringify(params), options).map(data => {

      let nearbyRestaurants = [];
      try {
        let response = data.json()
        switch (response.status) {
          case 200:
            let results = response.result;
            results.forEach(result => {
              let restaurant = new Restaurant(this.http);
              restaurant.initialize(result);
              nearbyRestaurants.push(restaurant);
            });
            return nearbyRestaurants;

          case 404:
            console.log('No results in your area!');
            break;
          default:
            console.log('Something went wrong');
        }
      }
      catch(error) {return []};

    });

  }

  getRestaurantByID(restaurantID:string) {
    let headers = new Headers()
    headers.append('x-api-key', this.apiKey);
    let options = new RequestOptions({ headers: headers });

    let url =  this.baseURL + "restaurants/" + restaurantID
    return this.http.get(url, options).map(data => {
      try {
        let response = data.json();
        switch (response.status) {
          case 200:
            let restaurant = new Restaurant(this.http);
            restaurant.initialize(response.result);
            return restaurant;
          case 404:
            console.log('No results in your area!');
            break;
          default:
            console.log('Something went wrong');
        }
      }
      catch(error) {return []};

    });
  }

  getNearbyDeliveryRestaurants() {
    let headers = new Headers()
    headers.append('x-api-key', this.apiKey);
    let options = new RequestOptions({ headers: headers });



    let url =  this.baseDeliveryURL + "delivery/restaurants"
    let params = {
      "latitude": this.locationManager.latitude,
      "longitude": this.locationManager.longitude,
      // "latitude": 40.756402,
      // "longitude": -74.053267,
      "userID": "eba886f8-9338-476a-b958-ad92efb18ef9",
      "sessionID": "7cd93c9d-5087-4468-a381-f5a2bc11ae78"
    }
    return this.http.post(url, JSON.stringify(params), options).map(data => {

      let nearbyRestaurants = [];
      let nearbyTopDishes = [];
      try {
        let response = data.json()
        switch (response.status) {
          case 200:
            let restaurants = response.result.restaurants;
            restaurants.forEach(r => {
              let restaurant = new DeliveryRestaurant(this.http);
              restaurant.initialize(r);
              nearbyRestaurants.push(restaurant);
            });

            let topDishes = response.result.topDishes;
            topDishes.forEach(t => {
              let topDish = new DeliveryTopDish();
              topDish.initialize(t);
              nearbyTopDishes.push(topDish);
            });
            return {'restaurants': nearbyRestaurants, 'topDishes': nearbyTopDishes};

          case 404:
            console.log('No results in your area!');
            break;
          default:
            console.log('Something went wrong');
        }
      }
      catch(error) {};

    });


  }


  createNewGroupUser(phoneNumber:string) {
    return firebase.database().ref('users/' + this.userID).update({'isOnline': true, 'name': this.userName})
  }

  getGroupRecommendedRestaurants(groupID:string) {
    let headers = new Headers()
    headers.append('x-api-key', this.apiKey);
    let options = new RequestOptions({ headers: headers });



    let url =  this.baseURL + "groups/recommendedRestaurants"
    let params = {
      "latitude": this.locationManager.latitude,
      "longitude": this.locationManager.longitude,
      "userID": this.userID,
      "groupID": groupID
    }
    return this.http.post(url, JSON.stringify(params), options).map(data => {
      let recommendedRestaurants = [];
      try {
        let response = data.json()
        switch (response.status) {
          case 200:
            let restaurants = response.result;
            restaurants.forEach(r => {
              let restaurant = new Restaurant(this.http);
              restaurant.initialize(r);
              recommendedRestaurants.push(restaurant);
            });

            return recommendedRestaurants;

          case 404:
            console.log('No results in your area!');
            break;
          default:
            console.log('Something went wrong');
        }
      }
      catch(error) {};

    });
  }

}
