
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http, Headers, RequestOptions } from '@angular/http';


@Injectable()
export class MasterDataProvider {
  
  baseURL = "https://ilbdbmc8sb.execute-api.us-east-1.amazonaws.com/v2/";
  baseDeliveryURL = "https://ur5sumi7vi.execute-api.us-east-1.amazonaws.com/v2/"
  apiKey = "50YL0BNXlc6I8NjvlXzxE3KmHFd829sJ24AwkXuO";
  
  allCuisines = {}
  allMeats = {}
  constructor(public http: Http) {}

  initialize(){
    let headers = new Headers()
    headers.append('x-api-key', this.apiKey);
    let options = new RequestOptions({ headers: headers });
    let url =  this.baseURL + "master-data";
    this.http.get(url, options).subscribe(data => {
      
      try {
        let response = data.json()
        switch (response.status) {
          case 200:
            response.result.dishCuisines.forEach(cuisine => {
              // let cuisineName = cuisine.dishCuisine.replace(' Restaurant', '')
              this.allCuisines[cuisine.dishCuisineID] = cuisine.dishCuisine.replace(' Restaurant', '');
            });
            response.result.dishMeats.forEach(meat => {
              this.allMeats[meat.dishMeatID] = meat.dishMeat;
            });
            break;
          default:
            console.log('Something went wrong');
        }
      } 
      catch(error) {return []};

    });
  }

}
