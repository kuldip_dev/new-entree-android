import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Firebase } from '@ionic-native/firebase'
import { Platform } from 'ionic-angular';

import * as firebase from 'firebase';
import { NativeStorage } from '@ionic-native/native-storage';

@Injectable()
export class FcmProvider {
  
  constructor(public http: HttpClient, public firebaseNative: Firebase, private platform: Platform, public nativeStorage:NativeStorage) {
    console.log('Hello FcmProvider Provider');
  }

  // Get permission from the user
  async getToken() {

    let token;
  
    if (this.platform.is('android')) {
      token = await this.firebaseNative.getToken()
    } 
  
    if (this.platform.is('ios')) {
      token = await this.firebaseNative.getToken();
      await this.firebaseNative.grantPermission();
    } 
    let existingToken = ""
    this.nativeStorage.getItem('existingToken').then(
      data => existingToken = data,
      error => {}
    );
    
    if (token && token != existingToken){
      return this.saveTokenToDatabase(token);
    }
    return;
  }

  // Save the token to firestore
  private saveTokenToDatabase(token) {
    if (!token) return;
    
    // const devicesRef = firebase.database().ref('notifications/' + this.groupID + '/messages').push(newMessage, err=>{console.log(err)});
      
  
    // const docData = { 
    //   token,
    //   userId: 'testUser',
    // }
  
    // return devicesRef.doc(token).set(docData)
  }

  // Listen to incoming FCM messages
  listenToNotifications() {}

}
