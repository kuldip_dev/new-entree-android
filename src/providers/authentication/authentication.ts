import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Firebase } from '@ionic-native/firebase'
import { Platform } from 'ionic-angular';

@Injectable()
export class AuthenticationProvider {

  constructor(public http: HttpClient, public firebase: Firebase) {
    console.log('Hello AuthenticationProvider Provider');
  }

  loginWithPhoneNumber(phoneNumber)  : any {
    // (<any>window).firebase.auth().verifyPhoneNumber("+123456789").then(function(verificationId) {
    //   // pass verificationId to signInWithVerificationId
    // });
    this.firebase.verifyPhoneNumber(phoneNumber,60)
      .then(function (verificationId) {
          console.log(verificationId)
          return verificationId;
      })
  }

doLoginWithPhoneNumber() {

  // this.firebase.auth().verifyPhoneNumber('+123456789')
  //     .then(function (verificationId) {
  //       var verificationCode = window.prompt('Please enter the verification ' +
  //         'code that was sent to your mobile device.');
  //       return this.firebase.auth.PhoneAuthProvider.credential(verificationId,
  //         verificationCode);
  //     })
  //     .then(function (phoneCredential) {
  //       return this.firebase.auth().signInWithCredential(phoneCredential);
  //     });
  }


}
