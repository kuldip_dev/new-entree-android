import { Injectable } from "@angular/core";
import 'rxjs/add/operator/map';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Restaurant } from '../../models/restaurant/restaurant'

@Injectable()
export class Dish {
  
  dishName:string;
  dishID:string;

  cuisineID:string;
  meatID:string;
  
  minPrice:number;
  maxPrice:number;
  priceString:string = "";
  
  images = [];
  restaurants = [];

  restaurantCount:number;
  
  baseURL = "https://ilbdbmc8sb.execute-api.us-east-1.amazonaws.com/v2/";
  apiKey = "50YL0BNXlc6I8NjvlXzxE3KmHFd829sJ24AwkXuO";
  
  constructor(public http: Http) {}

  initialize(dish:any) {
    this.dishID = dish.dishID;
    this.dishName = dish.dishName;

    this.cuisineID = dish.dishCuisineID;
    this.meatID = dish.dishMeatID;

    this.minPrice = dish.minPrice.toFixed(0);
    this.maxPrice = dish.maxPrice.toFixed(0);

    if (this.minPrice == this.maxPrice) {
      this.priceString = '$' + String(this.maxPrice);
    }
    else {
      this.priceString = '$' + String(this.minPrice) + ' - $' + String(this.maxPrice);
    }

    this.restaurantCount = dish.restaurantCount;
    
    this.images.push(dish.dishImage);

  }

  getDishDetails() {

    let headers = new Headers()
    headers.append('x-api-key', this.apiKey);
    let options = new RequestOptions({ headers: headers });

    let url =  this.baseURL + "dishes/dish"
    let params = {"latitude": 40.756402, "longitude": -74.053267, "dishID": this.dishID};
    this.http.post(url, JSON.stringify(params), options).subscribe(data => {
      try {
        let response = data.json()
        switch (response.status) {
          case 200:
            this.images = response.result.images;
            response.result.restaurants.forEach(element => {
              let restaurant = new Restaurant(this.http);
              restaurant.initialize(element, this.dishID);  
              this.restaurants.push(restaurant);
            }); 
            break;
          default:
            console.log('Something went wrong');
        }
      } 
      catch(error) {return []};

    });
  }


}
