import { Injectable } from "@angular/core";
import 'rxjs/add/operator/map';
import { Http, Headers, RequestOptions } from '@angular/http';

@Injectable()
export class Restaurant {

  name:string;
  restaurantID:string;
  partnerID:string;
  
  geohash:string;
  latitude:number;
  longitude:number;

  address:string;
  cuisine:string;
  distance:number;
  
  hasMenu:boolean = false;
  hasDrinks:boolean = false;

  restaurantURL?:string;

  hasReservation:boolean = false;
  reservationURL?:string;
  
  rating?:number;
  priceTier?:number;
  priceTierString:string = "";
  phoneNumber?:string;

  attributes = [];
  topDishes = [];
  filters = [];

  reviews = [];
  photos = [];
  menu = [];
  
  dishPrices = {};

  primaryPhotoURL?:string
  
  restaurantScore:number = 0.0;
  
  baseURL = "https://ilbdbmc8sb.execute-api.us-east-1.amazonaws.com/v2/";
  apiKey = "50YL0BNXlc6I8NjvlXzxE3KmHFd829sJ24AwkXuO";
  
  constructor(public http: Http) {}

  initialize(restaurant:any, selectedDish?:string) {
    try {
      this.name = restaurant.name;
      this.restaurantID = restaurant.restaurantID;
      this.partnerID = restaurant.partnerID;
      
      this.geohash = restaurant.geohash;
      this.latitude = restaurant.latitude;
      this.longitude = restaurant.longitude;

      this.address = restaurant.address;
      this.cuisine = restaurant.cuisine;

      if (restaurant.hasOwnProperty('distance')) {
        this.distance = restaurant.distance.toFixed(1);
      }
      else {
        this.distance = 1.3;
      }
      
      this.hasMenu = restaurant.hasMenu;
      this.hasDrinks = restaurant.hasDrinks;
      this.hasReservation = restaurant.hasReservation || false;

      if (this.hasReservation == true) {
        this.reservationURL = restaurant.reservationURL;
      }

      if (restaurant.hasOwnProperty('rating')) {
        this.rating = restaurant.rating.toFixed(1);
      }
      
      if (restaurant.hasOwnProperty('url')) {
        this.restaurantURL = restaurant.url;
      }

      if (restaurant.hasOwnProperty('priceTier')) {
        for (var i=0; i < restaurant.priceTier; i++) {
          this.priceTierString = this.priceTierString + "$";
        }
        
        this.priceTier = restaurant.priceTier;
      }
      else {
        this.priceTierString = " - "
      }

      if (restaurant.hasOwnProperty('phoneNumber')) {
        this.phoneNumber = restaurant.phoneNumber;
      }

      this.filters = restaurant.filters || [];
      
      restaurant.attributes.forEach(attribute => {
        if (attribute.attribute != 'Bar') {
          this.attributes.push(attribute.attribute);
        }
      });

      restaurant.topDishes.forEach(topDish => this.topDishes.push(topDish));

      this.restaurantScore = restaurant.restaurantScore;

      if (selectedDish != null) {
        if (restaurant.hasOwnProperty('dishPrice')) {
          this.dishPrices[selectedDish] = restaurant.dishPrice.toFixed(0);
        }
        else {
          this.dishPrices[selectedDish] = 0;
        }
      }

    } catch (error) {
      console.log(error);
    }
    

  }

  getRestaurantReviews() {

    let headers = new Headers()
    headers.append('x-api-key', this.apiKey);
    let options = new RequestOptions({ headers: headers });

    let url =  this.baseURL + "restaurants/" + this.restaurantID + "/reviews"
    
    this.http.get(url, options).subscribe(data => {
      
      try {
        let response = data.json()
        switch (response.status) {
          case 200:
            this.reviews = response.result;
            break;
          default:
            console.log('Something went wrong');
        }
      } 
      catch(error) {return []};

    });
  }

  getPrimaryPhoto() {
    let headers = new Headers()
    headers.append('x-api-key', this.apiKey);
    let options = new RequestOptions({ headers: headers });

    let url =  this.baseURL + "restaurants/" + this.restaurantID + "/images"
    
    this.http.get(url, options).subscribe(data => {
      
      try {
        let response = data.json()
        switch (response.status) {
          case 200:
            this.primaryPhotoURL = response.result[0].photoURL;
            break;
          case 404:
            break;
          default:
            console.log('Something went wrong');
        }
      } 
      catch(error) {};

    });
  }

  getRestaurantMenu() {
    
    let headers = new Headers()
    headers.append('x-api-key', this.apiKey);
    let options = new RequestOptions({ headers: headers });

    let url =  this.baseURL + "restaurants/" + this.restaurantID + "/menu"
    
    this.http.get(url, options).subscribe(data => {
      
      try {
        let response = data.json()
        switch (response.status) {
          case 200:
            console.log(response);
            this.menu = response.result;
            break;
          default:
            console.log('Something went wrong');
        }
      } 
      catch(error) {return []};

    });

  }

  getRestaurantPhotos() {
    
    let headers = new Headers()
    headers.append('x-api-key', this.apiKey);
    let options = new RequestOptions({ headers: headers });

    let url =  this.baseURL + "restaurants/" + this.restaurantID + "/images/all"
    
    this.http.get(url, options).subscribe(data => {
      
      try {
        let response = data.json()
        switch (response.status) {
          case 200:
            let photosResult = response.result;
            var leftSide = photosResult.filter(function(value, index, Arr) {
                return index % 2 == 1;
            });
            var rightSide = photosResult.filter(function(value, index, Arr) {
                return index % 2 == 0;
            });
            this.photos = leftSide.concat(rightSide);
            break;
          default:
            console.log('Something went wrong');
        }
      } 
      catch(error) {return []};

    });

  }

  private calculateDistance(lat1, lon1, lat2, lon2, unit) {
    var radlat1 = Math.PI * lat1/180
    var radlat2 = Math.PI * lat2/180
    var theta = lon1-lon2
    var radtheta = Math.PI * theta/180
    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    dist = Math.acos(dist)
    dist = dist * 180/Math.PI
    dist = dist * 60 * 1.1515
    if (unit=="K") { dist = dist * 1.609344 }
    if (unit=="N") { dist = dist * 0.8684 }
    return dist
  }

}
