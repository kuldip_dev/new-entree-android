import { Injectable } from "@angular/core";
import 'rxjs/add/operator/map';
import { Http, Headers, RequestOptions } from '@angular/http';

@Injectable()
export class DeliveryTopDish {
  dishID:string;
  dishName:string;
  dishCuisineID:string; 
  imageURL:string;
  dishScore:number;

  constructor () {}

  initialize(topDish:any) {
    this.dishID = topDish.dishID;
    this.dishName = topDish.dishName;
    this.dishCuisineID = topDish.dishCuisineID;
    this.imageURL = topDish.imageURL;
    this.dishScore = topDish.dishScore;
  }
}

@Injectable()
export class DeliveryRestaurant {

  name:string;
  restaurantID:string;
  partnerID:string;

  isOpen:boolean;
  
  latitude:number;
  longitude:number;

  address:string;
  distance:number;
  
  cuisines = [];
  primaryCuisine:string;

  acceptsCard:boolean;
  acceptsCash:boolean;

  deliveryFee:number;
  deliveryMin:number;
  taxRate:number;

  logoURL:string;
  
  minWaitTime:number;
  maxWaitTime:number;

  offersDelivery:boolean;
  offersPickup:boolean;
  
  rating?:number;
  priceTier?:number;
  priceTierString:string = "";
  phoneNumber?:string;
  
  dishes = {};
  menu = [];
  
  baseURL = "https://ur5sumi7vi.execute-api.us-east-1.amazonaws.com/v2/";
  apiKey = "50YL0BNXlc6I8NjvlXzxE3KmHFd829sJ24AwkXuO";
  
  constructor(public http: Http) {}

  initialize(restaurant:any) {
    this.name = restaurant.name;
    this.restaurantID = restaurant.restaurantID;
    this.partnerID = restaurant.partnerID;

    this.isOpen = restaurant.isOpen;
    
    this.latitude = restaurant.latitude;
    this.longitude = restaurant.longitude;

    this.address = restaurant.address;
    this.distance = restaurant.distance.toFixed(2);

    this.cuisines = restaurant.cuisines;
    this.primaryCuisine = restaurant.cuisines[0];
    
    this.acceptsCard = restaurant.acceptsCard;
    this.acceptsCard = restaurant.acceptsCash;

    this.deliveryFee = restaurant.deliveryFee;
    this.deliveryMin = restaurant.deliveryMin;
    this.taxRate = restaurant.taxRate;

    this.logoURL = restaurant.logoURL;

    this.minWaitTime = restaurant.minWaitTime;
    this.maxWaitTime = restaurant.maxWaitTime;

    this.offersDelivery = restaurant.offersDelivery;
    this.offersPickup = restaurant.offersPickup;

    if (restaurant.hasOwnProperty('rating')) {
      this.rating = restaurant.rating.toFixed(1);
    }
    
    if (restaurant.hasOwnProperty('priceTier')) {
      for (var i=0; i < restaurant.priceTier; i++) {
        this.priceTierString = this.priceTierString + "$";
      }
      
      this.priceTier = restaurant.priceTier;
    }
    else {
      this.priceTierString = " - "
    }

    if (restaurant.hasOwnProperty('phoneNumber')) {
      this.phoneNumber = restaurant.phoneNumber;
    }

    this.dishes = restaurant.dishes || {};

  }

  getRestaurantMenu() {
    
    let headers = new Headers()
    headers.append('x-api-key', this.apiKey);
    let options = new RequestOptions({ headers: headers });

    let url =  this.baseURL + "delivery/restaurants/menu"
    let params = {
      "restaurantID": "value3",
      "partnerID": this.partnerID
    }
    console.log(params);

    this.http.post(url, JSON.stringify(params), options).subscribe(data => {
      try {
        let response = data.json()
        switch (response.status) {
          case 200:
            this.menu = response.result;
            console.log(this.menu);
            break;
          default:
            console.log('Something went wrong');
        }
      } 
      catch(error) {console.log(error)};

    });

  }


}
